package com.hibernate.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernate.util.SessionFactoryUtil;
import com.hibernate.dto.Student;

public class HqlDAO {
	
     public List<Student> getStudents()
     {
    	/* Configuration configuration = new Configuration();
 		 configuration.configure();
 		 
 		 SessionFactory sessionFactory = configuration.buildSessionFactory();  */
 		 
    	 Session session = com.hibernate.util.SessionFactoryUtil.getSessionFactory().openSession();
 		 
 		 String hql="from Student";
 		 Query query = session.createQuery(hql);
 		 
 		 query.setCacheable(true);
 		 
 		 List<Student> list = query.list();
 		 return list;
 	
     }
     
     public List<Student> getStudentByContactNumber(Long contactNumber)
     {
 		Configuration configuration = new Configuration();
 		configuration.configure();
 		SessionFactory sessionFactory = configuration.buildSessionFactory();
 		Session session = sessionFactory.openSession();
 		String hql="from Student where contactNumber=:contact";
 		Query query = session.createQuery(hql);
 		query.setParameter("contact", contactNumber);
 		//Student student = (Student) query.uniqueResult();
 		//return student;
 		
 		 List<Student> list = query.list();
 		 return list;
 	}
     
     public void updateContactById(Long id,Long  contactNumber)
     {
    	Configuration configuration = new Configuration();
  		configuration.configure();
  		SessionFactory sessionFactory = configuration.buildSessionFactory();
  		Session session = sessionFactory.openSession();
  		String hql="update  Student set  contactNumber=:cont where id=:id";
  		Query query = session.createQuery(hql);
 		query.setParameter("cont", contactNumber);
 		query.setParameter("id", id);
 	    int rowcount = query.executeUpdate();
 		/*if(rowcount!=0)
 		{
 			System.out.println("Updated Successfully");
 		}
 		else
 		{
 			System.out.println("update fail");
 		}*/
    	 
     }
     
     public void deleteById(Long id)
     {
    	Configuration configuration = new Configuration();
   		configuration.configure();
   		SessionFactory sessionFactory = configuration.buildSessionFactory();
   		Session session = sessionFactory.openSession();
   		String hql="delete from  Student  where id=:id"; 
   		Query query = session.createQuery(hql);
   		query.setParameter("id", id);
   		int rowcount = query.executeUpdate();
 		if(rowcount!=0)
 		{
 			System.out.println("Deleted Successfully");
 		}
 		else
 		{
 			System.out.println("Delete operation  failed");
 		}
     }

}

