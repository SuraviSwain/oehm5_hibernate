package com.hibernate.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.hibernate.dto.Student;
import com.hibernate.util.SessionFactoryUtil;

public class StudentDAO {
	
	public void saveStudentDetails(Student student) {
		
	/*	Configuration configuration = new Configuration();
		configuration.configure();
		//configuration.addAnnotatedClass(Student.class);
		
		SessionFactory sessionFactory = configuration.buildSessionFactory();  */
		
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		
		Transaction transaction = session.beginTransaction();
		
		session.save(student);
		transaction.commit();
	}
	
	public Student getStudentById(Long id){
		
		Configuration configuration = new Configuration();
		configuration.configure();
		
		SessionFactory sessionFactory = configuration.buildSessionFactory();
		
		Session session = sessionFactory.openSession();
		
		Student student = session.get(Student.class, id);
		return student;
	}
	
	public void updateStudentById(Long id,String branch){
		
		Student student = getStudentById(id);
		
		
		if(student!=null){
			student.setBranch(branch);
			Configuration configuration = new Configuration();
			configuration.configure();
			
			SessionFactory sessionFactory = configuration.buildSessionFactory();
			
	        Session session = sessionFactory.openSession();
			
			Transaction transaction = session.beginTransaction();
			session.update(student);
			transaction.commit();
			System.out.println("update successful");
			return;
		}
		System.out.println("update unsuccessful");
	}
	
	public void deleteStudentById(Long id){
		
		Student student = getStudentById(id);
		
		if(student!=null){
			Configuration configuration = new Configuration();
			configuration.configure();
			
            SessionFactory sessionFactory = configuration.buildSessionFactory();
			
	        Session session = sessionFactory.openSession();
			
			Transaction transaction = session.beginTransaction();
			
			session.delete(student);
			transaction.commit();
			System.out.println("Record deleted");
		}
		System.out.println("Delete failed");
		}
		
	
}

