package com.hibernate.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.hibernate.dto.Brand;
import com.hibernate.dto.Captain;
import com.hibernate.dto.Country;
import com.hibernate.dto.Movie;
import com.hibernate.dto.State;
import com.hibernate.dto.Team;
import com.hibernate.util.SessionFactoryUtil;


public class AssociationDAO {

	public void saveTeamDetails(Team team) {
		
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(team);
		transaction.commit();
	}
	
	
public void saveCaptainDetails(Captain captain) {
		
		Session session = SessionFactoryUtil.getSessionFactory().openSession();
		Transaction transaction = session.beginTransaction();
		session.save(captain);
		transaction.commit();
	}

public void saveBrandDetails(Brand brand) {
	Session session = SessionFactoryUtil.getSessionFactory().openSession();
	Transaction transaction = session.beginTransaction();
	session.save(brand);
	transaction.commit();
}

public void saveStateDetails(State state)
{
	Session session = SessionFactoryUtil.getSessionFactory().openSession();
	Transaction transaction = session.beginTransaction();
	
	session.save(state);

	transaction.commit();
}

public void saveMovieDetails(Movie movie)
{
	Session session = SessionFactoryUtil.getSessionFactory().openSession();
	Transaction tx = session.beginTransaction();
	session.save(movie);
	tx.commit();
}



}
